# Docs

## Models
Stable Diffusion Models
Stable Diffusion is a latent text-to-image diffusion model capable of generating photo-realistic images given any text input. For more information about how Stable Diffusion works, please have a look at 🤗's Stable Diffusion with 🧨 Diffusers blog.
(Compvis)[https://ommer-lab.com/]
https://huggingface.co/CompVis

https://github.com/huggingface/diffusers

### Stable Diffusion
- https://clipdrop.co/stable-diffusion-turbo
- https://www.reddit.com/r/godot/comments/xgbdtb/stable_diffusion_ai_image_generation_in_godot/

### SDL Turbo

I am not sure it has been used 
(#TODO Check!)
https://huggingface.co/stabilityai/sdxl-turbo

### Hyper Stable Diffusion

- https://huggingface.co/ByteDance/Hyper-SD

### PEFT ( A Dependency )

- https://huggingface.co/docs/peft/en/install

### Background Removal

- https://huggingface.co/JCTN/RMBG-1.4
- https://huggingface.co/briaai/RMBG-1.4
- https://huggingface.co/spaces/briaai/BRIA-RMBG-1.4

## AMD GPU

- [AMD RADEON's support for tensor](https://rocm.docs.amd.com/projects/install-on-linux/en/latest/index.html)
- [Test the GPU is available](https://gist.github.com/damico/484f7b0a148a0c5f707054cf9c0a0533)

## Torch

- https://pytorch.org/get-started/locally/
- https://github.com/llvm/torch-mlir/

## PYQT

- [PYQT](https://www.pythonguis.com/tutorials/pyqt6-signals-slots-events/)
- [QT](https://doc.qt.io/qt-6/qwidget.html#details)
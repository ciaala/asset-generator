import math
import os
import shutil
import sys
import threading
import time
from typing import Optional, List

from PyQt6.QtCore import Qt, QEvent
from PyQt6.QtGui import QPixmap, QIcon, QMouseEvent
from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QGridLayout, QLabel, QVBoxLayout, \
    QHBoxLayout, QFileDialog

from src.app import ImageGenerationApp, generate_descriptions


class AssetSelector(QWidget):
    def __init__(self, base_folder: str) -> None:
        super().__init__()
        self._generator: Optional[ImageGenerationApp] = None
        self.base_folder = base_folder
        self.current_folder: Optional[str] = None
        self.create_missing_asset_folders()
        self.initUI()
        self.showFolders()
        self._thread = self._lazy_initialisation()
    def _long_construction(self) ->None:
        start = time.time()
        self._generator = ImageGenerationApp(self.base_folder)
        print(f'model loaded in {math.floor((time.time() - start)*100)/100} seconds', flush=True)


    # class GoBackButtonEventFilter(QObject):
    def _lazy_initialisation(self) -> threading.Thread:
        thread = threading.Thread(target=self._long_construction)
        thread.start()
        return thread

    @property
    def generator(self) -> ImageGenerationApp:
        self._thread.join()
        return self._generator

    def _mouse_event_filter(self, event: Optional[QMouseEvent]) -> None:
        if event and event.type() == QEvent.Type.MouseButtonPress:
            if event.button() == Qt.MouseButton.BackButton:
                self.onGoBack()

    def initUI(self) -> None:
        self.setGeometry(100, 100, 1920, 1080)
        self.setWindowTitle('Select assets for the card game')

        # Main layout
        self.mainLayout = QVBoxLayout()
        self.setLayout(self.mainLayout)

        # Top bar layout with buttons
        self.topBar = QHBoxLayout()
        self.exportButton = QPushButton('Export as Game Assets')
        self.exportButton.clicked.connect(self.onExportAssets)
        self.topBar.addWidget(self.exportButton)

        self.backButton = QPushButton('Back to Main')
        self.backButton.clicked.connect(self.onGoBack)
        self.topBar.addWidget(self.backButton)

        self.regenerateActionButton = QPushButton('Regenerate Images')
        self.regenerateActionButton.clicked.connect(self.onRegenerateAction)
        self.topBar.addWidget(self.regenerateActionButton)
        self.mainLayout.addLayout(self.topBar)

        # Grid layout for images and folders
        self.grid = QGridLayout()
        self.mainLayout.addStretch(1)
        self.setLayout(self.mainLayout)

        self.mainLayout.addLayout(self.grid)
        self.mainLayout.installEventFilter(self)
        self.mousePressEvent = lambda e: self._mouse_event_filter(e)

    def _hide_topBar(self) -> None:
        self.regenerateActionButton.hide()
        self.backButton.hide()

    def _show_topBar(self) -> None:
        self.regenerateActionButton.show()
        self.backButton.show()

    def showFolders(self):
        self._hide_topBar()
        self.clear_grid()
        row, col = 0, 0
        for folder_name in os.listdir(self.base_folder):
            folder_path = os.path.join(self.base_folder, folder_name)
            if os.path.isdir(folder_path):
                # Attempt to find the first image in the folder
                icon_path = self._find_folder_image(folder_path)
                btn = QPushButton(folder_name)
                # btn.setStyleSheet("padding: 16px;")
                btn.clicked.connect(lambda ch, path=folder_path: self.folderSelected(path))
                btn.setStyleSheet("height: 128px; font-size: 32px;")
                if icon_path:
                    pic = QPixmap(icon_path).scaled(128, 128, Qt.AspectRatioMode.KeepAspectRatio)
                    icon = QIcon(pic)
                    btn.setIconSize(pic.size())
                    btn.setIcon(icon)

                self.grid.addWidget(btn, row, col)
                col += 1
                if col >= 4:
                    row += 1
                    col = 0

    def _images_in_folder(self, folder_path: str) -> List[str]:
        files = os.listdir(folder_path)
        image_suffixes = ('.png', '.jpg', '.jpeg')
        images = list(filter(lambda file_name: file_name.lower().endswith(image_suffixes), files))
        return images

    def _find_folder_image(self, folder_path: str) -> Optional[str]:
        images = self._images_in_folder(folder_path)
        if not images:
            return os.path.join(self.base_folder, "squirrel.jpg")
        for file_name in images:
            if file_name.rsplit('.')[0].lower().endswith('_selected'):
                return os.path.join(folder_path, file_name)
        return None
        # os.path.join(folder_path, images[0])

    def folderSelected(self, folder_path: str) -> None:
        self.current_folder = folder_path
        self.displayImages(folder_path)

    def displayImages(self, folder_path: str) -> None:
        self._show_topBar()
        self.current_folder = folder_path
        # Clear existing images in the layout
        self.clear_grid()

        image_paths = self._images_in_folder(folder_path)
        # image_paths.sort(key=lambda file_name: not file_name.rsplit('.', 1)[0].endswith('_selected'))
        image_paths = [os.path.join(folder_path, f) for f in image_paths]
        for i, path in enumerate(image_paths):
            pixmap = QPixmap(path)
            label = QLabel(self)
            label.setPixmap(pixmap.scaled(256, 256, Qt.AspectRatioMode.KeepAspectRatio))
            label.setObjectName('imageLabel{}'.format(i))  # Unique object name for styling
            if path.rsplit('.', 1)[0].endswith('_selected'):
                label.setStyleSheet('border: 8px solid gold;')  # Default to no border
            else:
                label.setStyleSheet('border: 8px solid transparent;')  # Default to no border
            label.mousePressEvent = lambda event, p=path: self.onImageClick(event, p)
            self.grid.addWidget(label, i // 4, i % 4)  # Adjust based on your UI needs

    def clear_grid(self) -> None:
        for i in reversed(range(self.grid.count())):
            widget_to_remove = self.grid.itemAt(i).widget()
            self.grid.removeWidget(widget_to_remove)
            widget_to_remove.deleteLater()

    def _new_filename(self, image_path: str) -> str:
        filename = os.path.basename(image_path)
        new_filename = filename.rsplit('.')[0] + "_selected." + filename.rsplit('.')[1]
        folder = os.path.dirname(image_path)
        new_path = os.path.join(folder, new_filename)
        return new_path

    def onImageClick(self, event: QMouseEvent, image_path: str) -> None:
        if event.type() == QEvent.Type.MouseButtonPress.MouseButtonPress:
            if event.button() == Qt.MouseButton.LeftButton:
                # unselect_previous_image
                self._unselect_previous_images(os.path.dirname(image_path))
                if not image_path.rsplit('.', 1)[0].endswith('_selected'):
                    # select_new_image
                    new_path = self._new_filename(image_path)
                    os.renames(image_path, new_path)
                self.displayImages(self.current_folder)
            elif event.button() == Qt.MouseButton.BackButton:
                self.onGoBack()

    def onGoBack(self) -> None:
        self.showFolders()

    def onRegenerateAction(self) -> None:
        if self.current_folder:
            self.regenerate_images(self.current_folder)
            self.displayImages(self.current_folder)


    def onExportAssets(self) -> None:
        export_folder_path = QFileDialog.getExistingDirectory(self, "Select Directory")
        export_filenames: List[str]= []
        for folder_name in os.listdir(self.base_folder):
                folder_path = os.path.join(self.base_folder, folder_name)
                if os.path.isdir(folder_path):
                    # Attempt to find the first image in the folder
                    icon_path = self._find_folder_image(folder_path)
                    if icon_path:
                        new_filename = folder_name + "." + icon_path.rsplit('.', 1)[1]
                        print(f"Exporting: {new_filename}")
                        shutil.copy(icon_path, os.path.join(export_folder_path, new_filename))
                        export_filenames.append(new_filename)
        export_filenames.sort()
        for filename in export_filenames:
            print(f'load("res://assets/selected/{filename}"),')


    def _unselect_previous_images(self, folder_path: str) -> None:
        for image_path in self._images_in_folder(folder_path):
            if image_path.rsplit('.', 1)[0].endswith('_selected'):
                new_image_name = image_path.replace('_selected', '')
                os.renames(
                    os.path.join(folder_path, image_path),
                    os.path.join(folder_path, new_image_name))

    def regenerate_images(self, folder_path: str) -> None:
        descriptions = generate_descriptions()
        folder_name = os.path.basename(folder_path)
        description = next(filter(lambda desc: desc.key == folder_name, descriptions))
        self.generator.generate(description)

    def create_missing_asset_folders(self)->None:
        descriptions = generate_descriptions()
        for description in descriptions:
            folder = os.path.join(self.base_folder, description.key)
            os.makedirs(folder, exist_ok=True)


def main():
    app = QApplication(sys.argv)
    ex = AssetSelector('../../output')
    ex.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()

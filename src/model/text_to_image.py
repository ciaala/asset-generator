import abc

import torch
from PIL.Image import Image
from diffusers import DiffusionPipeline, DDIMScheduler, StableDiffusionXLPipeline, \
    StableDiffusionPipeline
from huggingface_hub import hf_hub_download


class TextToImage(abc.ABC):

    def generate(self, prompt: str) -> Image:
        raise NotImplementedError()


class HyperImageToText(TextToImage):
    def __init__(self, _pipeline: StableDiffusionXLPipeline) -> None:
        self._pipeline = _pipeline

    def generate(self, prompt: str) -> Image:
        return self._pipeline(prompt=prompt,
                              num_inference_steps=2,
                              guidance_scale=0).images[0]


class ImageDiffusionToText(TextToImage):
    def __init__(self, pipeline: StableDiffusionPipeline) -> None:
        self._pipeline = pipeline

    def generate(self, prompt: str) -> Image:
        image = self._pipeline(prompt).images[0]
        return image


class TextToImageFactory:
    def __init__(self, image_width: int, image_height: int) -> None:
        self.image_width = image_width
        self.image_height = image_height

    def stable_diffusion(self) -> TextToImage:
        model = "runwayml/stable-diffusion-v1-5"
        pipeline = DiffusionPipeline.from_pretrained(model, torch_dtype=torch.float16).to("cuda")
        return ImageDiffusionToText(pipeline)

    def hyper_sd(self) -> TextToImage:
        base_model_id = "stabilityai/stable-diffusion-xl-base-1.0"
        repo_name = "ByteDance/Hyper-SD"
        ckpt_name = "Hyper-SDXL-2steps-lora.safetensors"

        pipeline = DiffusionPipeline.from_pretrained(base_model_id, torch_dtype=torch.float16, variant="fp16").to(
            "cuda")
        pipeline.load_lora_weights(hf_hub_download(repo_name, ckpt_name))
        pipeline.fuse_lora()
        pipeline.scheduler = DDIMScheduler.from_config(pipeline.scheduler.config, timestep_spacing="trailing")
        return HyperImageToText(pipeline)

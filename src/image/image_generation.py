from typing import List

from PIL.Image import Image

from src.model.background import BackgroundRemoval
from src.model.text_to_image import TextToImage


class ImageGenerator:
    def __init__(self, text_to_image: TextToImage, background_removal: BackgroundRemoval) -> None:
        self.text_to_image = text_to_image
        self.background_removal = background_removal

    def generate(self, prompt: str, times: int) -> List[Image]:
        with_backgrounds: List[Image] = []
        for i in range(times):
            image_with_background = self.text_to_image.generate(prompt)
            with_backgrounds.append(image_with_background)
        images: List[Image] = self.background_removal.remove(with_backgrounds)
        # images.append(image)
        return images
